FROM java:8
VOLUME /tmp
ADD JXC-0.0.1-SNAPSHOT.war /app.war
EXPOSE 80
ENTRYPOINT ["java","-jar","/app.war"]

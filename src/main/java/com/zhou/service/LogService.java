package com.zhou.service;

import java.util.List;

import org.springframework.data.domain.Sort.Direction;

import com.zhou.entity.Log;

/**
 * 日志Service接口
 */
public interface LogService {

	/**
	 * 添加或者修改日志信息	 */
	public void save(Log log);
	
	/**
	 * 根据条件分页查询日志信息
	 */
	public List<Log> list(Log log,Integer page,Integer pageSize,Direction direction,String...properties);
	
	/**
	 * 获取总记录数
	 */
	public Long getCount(Log log);
}

package com.zhou.service;

import com.zhou.entity.UserRole;

/**
 * 用户角色关联service接口
 */
public interface UserRoleService {

	/**
	 * 根据用户id删除所有关联信息
	 */
	public void deleteByUserId(Integer userId);
	
	/**
	 * 添加或者修改用户角色关联
	 */
	public void save(UserRole userRole);
	
	/**
	 * 根据角色id删除所有关联信息
	 */
	public void deleteByRoleId(Integer roleId);
}

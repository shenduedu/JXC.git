package com.zhou.service;

import java.util.List;

import org.springframework.data.domain.Sort.Direction;

import com.zhou.entity.User;

/**
 * 用户Service接口
 */
public interface UserService {

	/**
	 * 根据用户名查找用户实体
	 */
	public User findByUserName(String userName);
	
	/**
	 * 根据条件分页查询用户信息
	 */
	public List<User> list(User user,Integer page,Integer pageSize,Direction direction,String...properties);
	
	/**
	 * 获取总记录数
	 */
	public Long getCount(User user);
	
	/**
	 * 添加或者修改用户信息
	 */
	public void save(User user);
	
	/**
	 * 根据id删除用户
	 */
	public void delete(Integer id);
	
	/**
	 * 根据id查询实体
	 */
	public User findById(Integer id);
}

package com.zhou.service;

import java.util.List;

import org.springframework.data.domain.Sort.Direction;

import com.zhou.entity.SaleList;
import com.zhou.entity.SaleListGoods;

/**
 * 销售单Service接口
 */
public interface SaleListService {

	/**
	 * 根据id查询实体
	 */
	public SaleList findById(Integer id);
	
	/**
	 * 获取当天最大销售单号
	 */
	public String getTodayMaxSaleNumber();
	
	/**
	 * 添加销售单 以及所有销售单商品  以及 修改 库存数量
	 */
	public void save(SaleList saleList,List<SaleListGoods> saleListGoodsList);
	
	/**
	 * 根据条件查询销售单信息
	 */
	public List<SaleList> list(SaleList saleList,Direction direction,String...properties);
	
	/**
	 * 根据id删除销售单信息 包括销售单里的所有商品
	 */
	public void delete(Integer id);
	
	/**
	 * 更新销售单
	 */
	public void update(SaleList saleList);
	
	/**
	 * 按天统计某个日期范围内的销售信息
	 */
	public List<Object> countSaleByDay(String begin,String end);
	
	/**
	 * 按月统计某个日期范围内的销售信息
	 */
	public List<Object> countSaleByMonth(String begin,String end);
}

package com.zhou.service;

import java.util.List;

import com.zhou.entity.SaleListGoods;

/**
 * 销售单商品Service接口
 */
public interface SaleListGoodsService {

	/**
	 * 根据销售单id查询所有销售单商品
	 */
	public List<SaleListGoods> listBySaleListId(Integer saleListId);
	
	/**
	 * 统计某个商品的销售总数
	 */
	public Integer getTotalByGoodsId(Integer goodsId);
	
	/**
	 * 根据条件查询销售单商品
	 */
	public List<SaleListGoods> list(SaleListGoods saleListGoods);
	
}

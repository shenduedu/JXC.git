package com.zhou.service;

import java.util.List;

import com.zhou.entity.GoodsUnit;

/**
 * 商品单位Service接口
 */
public interface GoodsUnitService {

	/**
	 * 查询所有商品单位信息
	 */
	public List<GoodsUnit> listAll();
	
	/**
	 * 添加或者修改商品单位信息
	 */
	public void save(GoodsUnit goodsUnit);
	
	/**
	 * 根据id删除商品单位
	 */
	public void delete(Integer id);
	
	/**
	 * 根据id查询实体
	 */
	public GoodsUnit findById(Integer id);
}

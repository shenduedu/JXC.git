package com.zhou.service;

import java.util.List;

import com.zhou.entity.DamageListGoods;

/**
 * 商品报损单商品Service接口
 *
 */
public interface DamageListGoodsService {

	/**
	 * 根据商品报损单id查询所有商品报损单商品
	 */
	public List<DamageListGoods> listByDamageListId(Integer damageListId);
	
}

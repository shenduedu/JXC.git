package com.zhou.service;

import java.util.List;

import org.springframework.data.domain.Sort.Direction;

import com.zhou.entity.Customer;

/**
 * 客户Service接口
 */
public interface CustomerService {

	/**
	 * 根据名称模糊查询客户信息
	 */
	public List<Customer> findByName(String name);
	
	/**
	 * 根据条件分页查询客户信息
	 */
	public List<Customer> list(Customer customer,Integer page,Integer pageSize,Direction direction,String...properties);
	
	/**
	 * 获取总记录数
	 */
	public Long getCount(Customer customer);
	
	/**
	 * 添加或者修改客户信息
	 */
	public void save(Customer customer);
	
	/**
	 * 根据id删除客户
	 */
	public void delete(Integer id);
	
	/**
	 * 根据id查询实体
	 */
	public Customer findById(Integer id);
}

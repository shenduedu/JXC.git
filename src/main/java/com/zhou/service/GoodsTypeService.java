package com.zhou.service;

import java.util.List;

import com.zhou.entity.GoodsType;

/**
 * 商品类别Service接口
 */
public interface GoodsTypeService {

	/**
	 * 根据父节点查找所有子节点
	 */
	public List<GoodsType> findByParentId(int parentId);
	
	/**
	 * 添加或者修改商品类别信息
	 */
	public void save(GoodsType goodsType);
	
	/**
	 * 根据id删除商品类别
	 */
	public void delete(Integer id);
	
	/**
	 * 根据id查询实体
	 */
	public GoodsType findById(Integer id);
}

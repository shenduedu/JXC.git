package com.zhou.service;

import java.util.List;

import org.springframework.data.domain.Sort.Direction;

import com.zhou.entity.PurchaseList;
import com.zhou.entity.PurchaseListGoods;

/**
 * 进货单Service接口
 */
public interface PurchaseListService {

	/**
	 * 根据id查询实体
	 */
	public PurchaseList findById(Integer id);
	
	/**
	 * 获取当天最大进货单号
	 */
	public String getTodayMaxPurchaseNumber();
	
	/**
	 * 添加进货单 以及所有进货单商品  以及 修改商品成本价 库存数量 上次进价
	 */
	public void save(PurchaseList purchaseList,List<PurchaseListGoods> purchaseListGoodsList);
	
	/**
	 * 根据条件查询进货单信息
	 */
	public List<PurchaseList> list(PurchaseList purchaseList,Direction direction,String...properties);
	
	/**
	 * 根据id删除进货单信息 包括进货单里的所有商品
	 */
	public void delete(Integer id);
	
	/**
	 * 更新进货单
	 */
	public void update(PurchaseList purchaseList);
}

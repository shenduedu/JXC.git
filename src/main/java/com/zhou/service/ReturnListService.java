package com.zhou.service;

import java.util.List;

import org.springframework.data.domain.Sort.Direction;

import com.zhou.entity.ReturnList;
import com.zhou.entity.ReturnListGoods;

/**
 * 退货单Service接口
 */
public interface ReturnListService {

	/**
	 * 获取当天最大退货单号
	 */
	public String getTodayMaxReturnNumber();
	
	/**
	 * 添加退货单 以及所有退货单商品  以及 修改 库存数量 
	 */
	public void save(ReturnList returnList,List<ReturnListGoods> returnListGoodsList);

	/**
	 * 根据id查询实体
	 */
	public ReturnList findById(Integer id);
	
	/**
	 * 根据条件查询退货单信息
	 */
	public List<ReturnList> list(ReturnList returnList,Direction direction,String...properties);
	
	/**
	 * 根据id删除退货单信息 包括退货单里的所有商品
	 */
	public void delete(Integer id);
	
	/**
	 * 更新退货单
	 */
	public void update(ReturnList returnList);

}

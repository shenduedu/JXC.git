package com.zhou.service;

import java.util.List;

import org.springframework.data.domain.Sort.Direction;

import com.zhou.entity.Supplier;

/**
 * 供应商Service接口
 */
public interface SupplierService {

	/**
	 * 根据名称模糊查询供应商信息
	 */
	public List<Supplier> findByName(String name);
	
	/**
	 * 根据条件分页查询供应商信息
	 */
	public List<Supplier> list(Supplier supplier,Integer page,Integer pageSize,Direction direction,String...properties);
	
	/**
	 * 获取总记录数
	 */
	public Long getCount(Supplier supplier);
	
	/**
	 * 添加或者修改供应商信息
	 */
	public void save(Supplier supplier);
	
	/**
	 * 根据id删除供应商
	 */
	public void delete(Integer id);
	
	/**
	 * 根据id查询实体
	 */
	public Supplier findById(Integer id);
}

package com.zhou.service;

import java.util.List;

import com.zhou.entity.ReturnListGoods;

/**
 * 退货单商品Service接口
 */
public interface ReturnListGoodsService {

	/**
	 * 根据退货单id查询所有退货单商品
	 */
	public List<ReturnListGoods> listByReturnListId(Integer returnListId);
	
	/**
	 * 根据条件查询退货单商品
	 */
	public List<ReturnListGoods> list(ReturnListGoods returnListGoods);
}

package com.zhou.service;

import java.util.List;

import org.springframework.data.domain.Sort.Direction;

import com.zhou.entity.CustomerReturnList;
import com.zhou.entity.CustomerReturnListGoods;

/**
 * 客户退货单Service接口
 */
public interface CustomerReturnListService {

	/**
	 * 根据id查询实体
	 */
	public CustomerReturnList findById(Integer id);
	
	/**
	 * 获取当天最大客户退货单号
	 */
	public String getTodayMaxCustomerReturnNumber();
	
	/**
	 * 添加客户退货单 以及所有客户退货单商品  以及 修改 库存数量
	 */
	public void save(CustomerReturnList customerReturnList,List<CustomerReturnListGoods> customerReturnListGoodsList);
	
	/**
	 * 根据条件查询客户退货单信息
	 */
	public List<CustomerReturnList> list(CustomerReturnList customerReturnList,Direction direction,String...properties);
	
	/**
	 * 根据id删除客户退货单信息 包括客户退货单里的所有商品
	 */
	public void delete(Integer id);
	
	/**
	 * 更新客户退货单
	 */
	public void update(CustomerReturnList customerReturnList);
	
	
}

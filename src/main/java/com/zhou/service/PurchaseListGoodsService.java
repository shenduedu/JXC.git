package com.zhou.service;

import java.util.List;

import com.zhou.entity.PurchaseListGoods;

/**
 * 进货单商品Service接口
 */
public interface PurchaseListGoodsService {

	/**
	 * 根据进货单id查询所有进货单商品
	 */
	public List<PurchaseListGoods> listByPurchaseListId(Integer purchaseListId);
	
	/**
	 * 根据条件查询进货单商品
	 */
	public List<PurchaseListGoods> list(PurchaseListGoods purchaseListGoods);
	
}

package com.zhou.service;

import java.util.List;

import com.zhou.entity.OverflowListGoods;

/**
 * 商品报溢单商品Service接口
 */
public interface OverflowListGoodsService {

	/**
	 * 根据商品报溢单id查询所有商品报溢单商品
	 */
	public List<OverflowListGoods> listByOverflowListId(Integer overflowListId);
	
}

package com.zhou.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.zhou.entity.GoodsUnit;

/**
 * 商品单位Repository接口
 */
public interface GoodsUnitRepository extends JpaRepository<GoodsUnit, Integer>{

	
}

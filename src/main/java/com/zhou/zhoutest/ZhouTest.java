package com.zhou.zhoutest;

import com.zhou.util.StringUtil;

import java.math.BigInteger;

public class ZhouTest {


    private static void toBinary(int b){
        System.out.println(Integer.toBinaryString(b));
    }

    private static void toBinary(int a,int b){
        System.out.println(Integer.toBinaryString(a)+Integer.toBinaryString(b));
    }

    private static String[] binaryArray =
            {"0000","0001","0010","0011",
                    "0100","0101","0110","0111",
                    "1000","1001","1010","1011",
                    "1100","1101","1110","1111"};

    public static String bytes2BinaryStr(byte[] bArray){

        String outStr = "";
        int pos = 0;
        for(byte b:bArray){
            //高四位
            pos = (b&0xF0)>>4;
            outStr+=binaryArray[pos];
            //低四位
            pos=b&0x0F;
            outStr+=binaryArray[pos];
        }
        return outStr;

    }

    public static BigInteger binaryToDecimal(String binarySource) {
        BigInteger bi = new BigInteger(binarySource, 2);    //转换为BigInteger类型
//         return Integer.parseInt(bi.toString());     //转换成十进制
        return bi;     //转换成十进制
    }

    public static void printComplementCode(int a)
    {
        for (int i = 0; i < 32; i++)
        {
            // 0x80000000 是一个首位为1，其余位数为0的整数
            int t = (a & 0x80000000 >>> i) >>> (31 - i);
            System.out.print(t);
        }
        System.out.println();
    }

    private static byte[] SumCheck(byte[] msg, int length) {
        long mSum = 0;
        byte[] mByte = new byte[length];

        /** 逐Byte添加位数和 */
        for (byte byteMsg : msg) {
            long mNum = ((long)byteMsg >= 0) ? (long)byteMsg : ((long)byteMsg + 256);
            mSum += mNum;
        } /** end of for (byte byteMsg : msg) */

        /** 位数和转化为Byte数组 */
        for (int liv_Count = 0; liv_Count < length; liv_Count++) {
            mByte[length - liv_Count - 1] = (byte)(mSum >> (liv_Count * 8) & 0xff);
        } /** end of for (int liv_Count = 0; liv_Count < length; liv_Count++) */

        return mByte;
    }

    public static void main(String[] args) {
        toBinary((int)'a');
        toBinary((int)'b');
        toBinary((int)'c');
        toBinary((int)'d');
        toBinary((int)'e');

        toBinary(97+98);

        toBinary((int)'a',(int)'b');
        toBinary((int)'c',(int)'d');


        System.out.println(bytes2BinaryStr("ab".getBytes()));

        System.out.println(binaryToDecimal(bytes2BinaryStr("e".getBytes())));

        System.out.println(24930+25444+101);

        System.out.println(Integer.toBinaryString(50475));

        System.out.println(new String(SumCheck("abcde".getBytes(), 16)));
    }


}

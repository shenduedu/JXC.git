package com.zhou.controller.admin;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zhou.entity.Log;
import com.zhou.entity.ReturnList;
import com.zhou.entity.ReturnListGoods;
import com.zhou.service.LogService;
import com.zhou.service.ReturnListGoodsService;
import com.zhou.service.ReturnListService;
import com.zhou.service.UserService;
import com.zhou.util.DateUtil;
import com.zhou.util.StringUtil;

/**
 * 后台管理退货单Controller
 */
@RestController
@RequestMapping("/admin/returnList")
public class ReturnListAdminController {

	@Resource
	private ReturnListService returnListService;
	
	@Resource
	private ReturnListGoodsService returnListGoodsService;
	
	@Resource
	private UserService userService;
	
	@Resource
	private LogService logService;
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(true);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));   //true:允许输入空值，false:不能为空值
	}
	
	/**
	 * 获取退货单号
	 */
	@RequestMapping("/genCode")
	@RequiresPermissions(value="退货出库")
	public String genCode()throws Exception{
		StringBuffer code=new StringBuffer("TH");
		code.append(DateUtil.getCurrentDateStr());
		String returnNumber=returnListService.getTodayMaxReturnNumber();
		if(returnNumber!=null){
			code.append(StringUtil.formatCode(returnNumber));
		}else{
			code.append("0001");
		}
		return code.toString();
	}
	
	/**
	 * 添加退货单 以及所有退货单商品
	 */
	@RequestMapping("/save")
	@RequiresPermissions(value="退货出库")
	public Map<String,Object> save(ReturnList returnList,String goodsJson)throws Exception{
		Map<String,Object> resultMap=new HashMap<>();
		returnList.setUser(userService.findByUserName((String) SecurityUtils.getSubject().getPrincipal())); // 设置操作用户
		Gson gson=new Gson();
		List<ReturnListGoods> rlgList=gson.fromJson(goodsJson,new TypeToken<List<ReturnListGoods>>(){}.getType());
		returnListService.save(returnList, rlgList);
		logService.save(new Log(Log.ADD_ACTION,"添加退货单"));
		resultMap.put("success", true);
		return resultMap;
	}
	
	/**
	 * 根据条件查询所有退货单信息
	 */
	@RequestMapping("/list")
	@RequiresPermissions(value={"退货单据查询","供应商统计"},logical=Logical.OR)
	public Map<String,Object> list(ReturnList returnList)throws Exception{
		Map<String,Object> resultMap=new HashMap<>();
		List<ReturnList> returnListList=returnListService.list(returnList, Direction.DESC, "returnDate");
		resultMap.put("rows", returnListList);
		logService.save(new Log(Log.SEARCH_ACTION,"退货单查询"));
		return resultMap;
	}
	
	/**
	 * 根据条件获取商品采购信息
	 */
	@RequestMapping("/listCount")
	@RequiresPermissions(value="商品采购统计")
	public Map<String,Object> listCount(ReturnList returnList,ReturnListGoods returnListGoods)throws Exception{
		Map<String,Object> resultMap=new HashMap<>();
		List<ReturnList> returnListList=returnListService.list(returnList, Direction.DESC, "returnDate");
		for(ReturnList rl:returnListList){
			returnListGoods.setReturnList(rl);
			List<ReturnListGoods> rlgList=returnListGoodsService.list(returnListGoods);
			rl.setReturnListGoodsList(rlgList);
		}
		resultMap.put("rows", returnListList);
		logService.save(new Log(Log.SEARCH_ACTION,"商品采购统计查询"));
		return resultMap;
	}
	
	/**
	 * 根据退货单id查询所有退货单商品
	 */
	@RequestMapping("/listGoods")
	@RequiresPermissions(value="退货单据查询")
	public Map<String,Object> listGoods(Integer returnListId)throws Exception{
		if(returnListId==null){
			return null;
		}
		Map<String,Object> resultMap=new HashMap<>();
		resultMap.put("rows", returnListGoodsService.listByReturnListId(returnListId));
		logService.save(new Log(Log.SEARCH_ACTION,"退货单商品查询"));
		return resultMap;
	}
	
	/**
	 * 删除退货单 以及退货单里的商品
	 */
	@RequestMapping("/delete")
	@RequiresPermissions(value="退货单据查询")
	public Map<String,Object> delete(Integer id)throws Exception{
		Map<String,Object> resultMap=new HashMap<>();
		returnListService.delete(id);
		logService.save(new Log(Log.DELETE_ACTION,"删除退货单信息："+returnListService.findById(id)));
		resultMap.put("success", true);
		return resultMap;
	}
	
	/**
	 * 修改退货单的支付状态
	 */
	@RequestMapping("/update")
	@RequiresPermissions(value="供应商统计")
	public Map<String,Object> update(Integer id)throws Exception{
		Map<String,Object> resultMap=new HashMap<>();
		ReturnList returnList=returnListService.findById(id);
		returnList.setState(1);
		returnListService.update(returnList);
		resultMap.put("success", true);
		return resultMap;
	}
}
